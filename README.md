# Brachistochrone mini lecture

A collection of supporting files for a mini lecture on the Brachistochrone challenge for 1st year Mathematics undergrads.

## Contents:

1. Presentation slides in Keynote (recommended) and PDF format.

2. A link to a Cycloid Demonstration through a computer applet developed by Brian Sterr in GeoGebra. Includes parameterised equations x(t) and y(t) for the cycloid and allows control of the generating circle radius R and integration time t. https://www.geogebra.org/m/QeQ9aA5e

3. A physics demonstration from Vanderbilt University of the cycloid path described by a laser beam through a vertical mass density gradient medium (and the recipe to reproduce it!). https://www.vanderbilt.edu/physicsdemonstration/davesdemos/demonstrations/demo091.htm

4. A modern solution of the brachistochrone challenge using Calculus of Variations. Includes an analytical solution for the case when friction is accounted for. https://mathworld.wolfram.com/BrachistochroneProblem.html

5. A really interesting chat between Grant Sanderson (3Blue1Brown) and Steven Strogatz about the brachistochrone challenge, including cool live animations: https://www.youtube.com/watch?v=Cld0p3a43fU

6. A Python program (brachistochrone.py) originally developed by Christian Hill that plots the arc of a cycloid, calculates the time of travel from P_1 to P_2, and compares it with three other paths: (i) a straight line, (ii) a circular path with a vertical tangent at P_1, and (iii) a parabola with vertical tangent at P_1.

7. Additional reading materials as follows:

- Babb, Jeff and Currie, James (2008) "The Brachistochrone Problem: Mathematics for a Broad Audience via a Large Context Problem". The Mathematics Enthusiast: Vol. 5 : No. 2 , Article 2. Includes Jakob Bernoulli's original solution using infinitesimal triangles.
- Levi, Mark (2014) "Quick! Find a Solution to the Brachistochrone Problem". SIAM News 48, no. 6. Includes what's possible the quickest solution of the brachistochrone (using Calculus of Variations) and a clever physical reasoning to determine the equation of the cycloid.
- Brookfield, Gary (2010) "Yet Another Elementary Solution of the Brachistochrone Problem", Mathematics Magazine, Vol. 83, No. 1, pp. 59-63. A solution to the brachistochrone that is general enough to prove that the cycloid arc yields the minimum travel time, not just among curves that are smooth, but also among curves that have loops and corners.
